/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.pucrio.les.mas.publisher.application01.PostOffice;
import br.pucrio.les.mas.publisher.application01.exceptions.BookIsNotInPostOffice;
import br.pucrio.les.mas.publisher.application01.exceptions.NothingForClientInPostOffice;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nathalianascimento
 */
public class PostOfficeTesting {
    
    PostOffice post = PostOffice.getInstance();
    public PostOfficeTesting() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
     public void addBook() {
        try {
            String nameClient1 = "client1";
            String nameClient2 = "client2";
            String nameBook1 = "MAS";
            String nameBook2 = "Test";
            
            post.addBookToDelivery(nameClient1, nameBook1);
            post.addBookToDelivery(nameClient2, nameBook1);
            post.addBookToDelivery(nameClient1, nameBook2);
            //post.addBookToDelivery(nameClient2, nameBook2);
            
            String[] booksClient = {nameBook1,nameBook2};
            assertArrayEquals(booksClient, post.removeBookList(nameClient1).toArray());
            assertEquals(nameBook1, post.removeBook(nameClient2, nameBook1));
         //   assertArrayEquals(booksClient, post.removeBookList(nameClient2).toArray());
        } catch (NothingForClientInPostOffice ex) {
            System.out.println("Exception01");
            //Logger.getLogger(PostOfficeTesting.class.getName()).log(Level.SEVERE, null, ex);
            //ex.getMessage();
        } catch (BookIsNotInPostOffice ex) {
            System.out.println("Exception02");
            Logger.getLogger(PostOfficeTesting.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
    
}
