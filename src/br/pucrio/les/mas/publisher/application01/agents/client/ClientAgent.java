/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01.agents.client;

import br.pucrio.les.mas.publisher.application01.PostOffice;
import br.pucrio.les.mas.publisher.application01.agents.seller.LogSeller;
import br.pucrio.les.mas.publisher.application01.agents.seller.SellerAgent;
import br.pucrio.les.mas.publisher.application01.exceptions.BookIsNotInPostOffice;
import br.pucrio.les.mas.publisher.application01.exceptions.NothingForClientInPostOffice;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.pucrio.les.mas.publisher.framework.TestableAgent;

/**
 *
 * @author Nathalia
 */
public class ClientAgent extends TestableAgent {

    PostOffice postman = PostOffice.getInstance();
    private String BookTitle = null;
    private boolean boughtBook = false;
    private AID[] SellingAgentsList;
    
    private boolean requiredTermination = false;

    public ClientAgent() {
        super();
    }
    
    public ClientAgent(String book) {
        super();
        this.BookTitle = book;
    }
    
    
    @Override
    protected void setup() {

        super.setup();
        sendLog(LogClient.Action.connectToSystem, LogClient.TypeLog.INFO,LogClient.MethodName.setup,"41", LogClient.Resource.agent, "");
                     
        
        if(BookTitle==null){
            Object[] args = getArguments();
             BookTitle = (String) args[0];
        }
        if (BookTitle!=null) {
            //1 minute
            addBehaviour(new TickerBehaviour(this, 60000) {
                @Override
                protected void onTick() {
                sendLog(LogClient.Action.searchBook, LogClient.TypeLog.INFO,LogClient.MethodName.setup,"53", LogClient.Resource.book, "book: "+BookTitle);
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("book-seller");
                template.addServices(sd);
                try {
                    SearchConstraints getAll = new SearchConstraints();
                    getAll.setMaxResults(new Long(-1));
                    //max number of replies (-1 means ALL)
                   DFAgentDescription[] result = DFService.search(myAgent, template, getAll);
                   sendLog(LogClient.Action.receiveListFromDF, LogClient.TypeLog.INFO,LogClient.MethodName.setup,"73", LogClient.Resource.yellowpage, "");
           
                   //book-seller list
                        SellingAgentsList = new AID[result.length];
                        for (int i = 0; i < result.length; ++i) {
                            SellingAgentsList[i] = result[i].getName();
                           // System.out.println(SellingAgentsList[i].getName());
                        }
                    } catch (FIPAException fe) {
                        System.out.println("DF IS DIED");
                        
                   sendLog(LogClient.Action.receiveListFromDF, LogClient.TypeLog.ERROR,LogClient.MethodName.setup,"60", LogClient.Resource.yellowpage, "");
                       // fe.printStackTrace();
                    }

                    myAgent.addBehaviour(new ClientAgent.buyBook());
                }
            });
            
            //addBehaviour(new ClientAgent.getBookInPostOffice(this,6000));
        } else {
            sendLog(LogClient.Action.searchBook, LogClient.TypeLog.WARNING,LogClient.MethodName.setup,"79", LogClient.Resource.book, "book not available");
            this.requiredTermination = true;
            takeDown();
        }
    }
    
    public void printAdresses(){
        String[] adresses = this.getAID().getAddressesArray();
        for(int c = 0; c< adresses.length; c++){
            System.out.println("Adress>>::: "+ c);
            System.out.println(adresses[c]);
        }
    }
    
     // Put agent clean-up operations here
    protected void takeDown() {
        // Printout a dismissal message
       // if(requiredTermination)
        sendLog(LogClient.Action.isDestroyed, LogClient.TypeLog.WARNING,LogClient.MethodName.takeDown,"110", LogClient.Resource.agent, "");
       
      //  else
       // sendLog(LogClient.Action.isDestroyed, LogClient.TypeLog.ERROR,LogClient.MethodName.takeDown,"110", LogClient.Resource.agent, "");

        doDelete();
    }

    @Override
    public String getNameClass() {
        return this.getClass().getSimpleName();    
    }


    private class buyBook extends Behaviour {

        private AID bestSeller; // seller with the best price
        private int bestPrice;  // best price
        private int repliesCnt = 0; // number of sellers that answered
        private MessageTemplate mt; // answer template
        private int step = 0;

        @Override
        public void action() {
            switch (step) {
                case 0:
                    //send message to all book-sellers 
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    cfp.clearAllReceiver();
                    for (int i = 0; i < SellingAgentsList.length; ++i) {
                        cfp.addReceiver(SellingAgentsList[i]);
                       // sendLog(LogClient.Action.addReceiver, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"138", LogClient.Resource.agent, SellingAgentsList[i].getLocalName());
        
                    }
                    cfp.setContent(BookTitle);
                    cfp.setConversationId("book-sellers");
                    cfp.setReplyWith("cfp" + System.currentTimeMillis()); 
                    myAgent.send(cfp);
                    sendLog(LogClient.Action.askPrice, LogClient.TypeLog.INFO,
                            LogClient.MethodName.buyBook,"145", 
                            LogClient.Resource.book, 
                            "ask book-price to "+SellingAgentsList.length +"sellers");
        
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("book-sellers"),
                            MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                case 1:
                    //WAIT MESSAGE FROM ALL SELLERS TO ANSWER IF THEY HAVE
                    //BOOK
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null) {
                        String nameSeller = reply.getSender().getLocalName();
                        if (reply.getPerformative() == ACLMessage.PROPOSE) {
                            int price = Integer.parseInt(reply.getContent());
                            if (bestSeller == null || price < bestPrice) {
                                bestPrice = price;
                                bestSeller = reply.getSender();
                            }
                            
                            sendLog(LogClient.Action.receivedBudget, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"163", LogClient.Resource.agent, "seller: "+nameSeller+" price: "+price);

                        }
                        else{
                           // sendLog(LogClient.Action.receivedRefusedBudget, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"174", LogClient.Resource.agent, "seller: "+nameSeller);

                        }
                        //else  seller refused budget
                        repliesCnt++;
                        if (repliesCnt >= SellingAgentsList.length) {
                            step = 2;
                        }
                    } else {
                     //   sendLog(LogClient.Action.waitSeller, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"187", LogClient.Resource.agent, "wait seller answer-budget");

                        block();
                    }
                    break;
                case 2:
                   
                   // sendLog(LogClient.Action.selectBestPrice, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"145", LogClient.Resource.agent, "seller: "+bestSeller+" price: "+bestPrice);

                    ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    order.addReceiver(bestSeller);
                    order.setContent(BookTitle);
                    order.setConversationId("book-sellers");
                    order.setReplyWith("Orden" + System.currentTimeMillis());
                    myAgent.send(order);
                    sendLog(LogClient.Action.askBuy, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"145", LogClient.Resource.agent, "seller: "+bestSeller.getLocalName()+" price: "+bestPrice);

                    
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("book-sellers"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    step = 3;
                    break;
                case 3:
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        
                        if (reply.getPerformative() == ACLMessage.INFORM) {
                           sendLog(LogClient.Action.receivedSaleConfirmation, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"180", LogClient.Resource.book, "seller: "+reply.getSender().getLocalName()+" price: "+bestPrice);
                           boughtBook = true; 
                           
                           /*After receiving sale confirmation, the client agent will search the book in the PostOffice*/
                           //2 seconds
                           addBehaviour(new ClientAgent.getBookInPostOffice(this.myAgent,4000));
                           
                        } else {
                            sendLog(LogClient.Action.receivedSaleConfirmation, LogClient.TypeLog.ERROR,LogClient.MethodName.buyBook,"180", LogClient.Resource.book, "book is not available");
                           
                        }

                        step = 4;
                    } else {
                       // sendLog(LogClient.Action.waitSeller, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"219", LogClient.Resource.agent, "wait seller answer-buy");

                        block();
                    }
                    break;
            }
        }

        @Override
        public boolean done() {
            if (step == 2) {
                if(bestSeller == null)
                 sendLog(LogClient.Action.selectBestPrice, LogClient.TypeLog.WARNING,LogClient.MethodName.buyBook,"183", LogClient.Resource.agent, "none of the "+repliesCnt+" has the book "+BookTitle);
                else
                 sendLog(LogClient.Action.selectBestPrice, LogClient.TypeLog.INFO,LogClient.MethodName.buyBook,"182", LogClient.Resource.agent, "seller: "+bestSeller.getLocalName()+" price: "+bestPrice);
             
            }
            return ((step == 2 && bestSeller == null) || step == 4);
        }
    }
    
     public class getBookInPostOffice extends TickerBehaviour{

            
            public getBookInPostOffice(Agent a, long period) {
                super(a, period);
            }

            @Override
            protected void onTick() {
              if(boughtBook){
                  String agentName = this.getAgent().getAID().getLocalName();
                  try {
                      postman.removeBook(agentName, BookTitle);
                      sendLog(LogClient.Action.receivedBook, LogClient.TypeLog.INFO,LogClient.MethodName.getBookInPostOffice,"229", LogClient.Resource.book, "book: "+BookTitle);
         
                      myAgent.doDelete();
                  } catch (BookIsNotInPostOffice ex) {
                      sendLog(LogClient.Action.receivedBook, LogClient.TypeLog.ERROR,LogClient.MethodName.getBookInPostOffice,"229", LogClient.Resource.book, "book: "+BookTitle);
         
                  } catch (NothingForClientInPostOffice ex) {
                      sendLog(LogClient.Action.receivedBook, LogClient.TypeLog.ERROR,LogClient.MethodName.getBookInPostOffice,"229", LogClient.Resource.book, "book: "+BookTitle);
         
                  }
                  
              }
                                 
            }   
        }//class end

    
}
