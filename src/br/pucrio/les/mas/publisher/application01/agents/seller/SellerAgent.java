/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01.agents.seller;

//import fiot.agents.behaviors.CreateNewAdaptiveAgent;

import br.pucrio.les.mas.publisher.application01.PostOffice;
import br.pucrio.les.mas.publisher.application01.exceptions.ProblemInServiceRegister;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.pucrio.les.mas.publisher.framework.LogValues;
import br.pucrio.les.mas.publisher.framework.TestableAgent;

//import fiot.agents.message.AgentAdress;

/**
 * Responsible to verify open IPs and create new Agents
 *
 * @author Nathalia
 */
public class SellerAgent extends TestableAgent {

	private HashMap catalogue;
        
        private HashMap booksToPost;
        
        private PostOffice postman;
	
        //Gui
	private BookSaleGui addBooksGui;
        boolean insertDefects = false;

    public SellerAgent(boolean insertDefects) {
        super();
        this.insertDefects = insertDefects;
    }

         @Override
    protected void setup() {

        super.setup();
        sendLog(LogSeller.Action.connectToSystem, LogSeller.TypeLog.INFO,LogSeller.MethodName.setup,"56", LogSeller.Resource.agent, "");
       
       // this.printAdresses();
	catalogue = new HashMap();
        booksToPost = new HashMap();
        postman = PostOffice.getInstance();
        
        sendLog(LogSeller.Action.createCatalogue, LogSeller.TypeLog.INFO,LogSeller.MethodName.setup,"55", LogSeller.Resource.catalogue, "");

	addBooksGui = new BookSaleGui(this);
        //addBooksGui.showGui();

	registerService();
        
	addBehaviour(new receiveBudgetRequest());


	addBehaviour(new receiveMsgToBuy());
        
        //ms
        if(this.insertDefects){
            addBehaviour(new DefectsendBooksToPostOffice(this,1800));
        }
        else{
            addBehaviour(new sendBooksToPostOffice(this,1800));
        }
        
        
        
    }
    
    protected void registerService(){
        // register as book-seller in Yellow Page
	DFAgentDescription dfd = new DFAgentDescription();
	dfd.setName(getAID());
	ServiceDescription sd = new ServiceDescription();
	sd.setType("book-seller");
	sd.setName("Yellow Page");
	dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
            sendLog(LogSeller.Action.registerService, LogSeller.TypeLog.INFO,LogSeller.MethodName.registerService,"71", LogSeller.Resource.yellowpage, "");

        } catch (FIPAException ex) {
            sendLog(LogSeller.Action.registerService, LogSeller.TypeLog.ERROR,LogSeller.MethodName.registerService,"71", LogSeller.Resource.yellowpage, "");

               // Logger.getLogger(SellerAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void printAdresses(){
        String[] adresses = this.getAID().getAddressesArray();
        for(int c = 0; c< adresses.length; c++){
            System.out.println("Adress>>::: "+ c);
            System.out.println(adresses[c]);
        }
    }

        @Override
	protected void takeDown() {
		try {
			DFService.deregister(this);
                        sendLog(LogSeller.Action.isDestroyed, LogSeller.TypeLog.WARNING,LogSeller.MethodName.takeDown,"98", LogSeller.Resource.agent, "");

		}
		catch (FIPAException fe) {
                        sendLog(LogSeller.Action.isDestroyed, LogSeller.TypeLog.ERROR,LogSeller.MethodName.takeDown,"98", LogSeller.Resource.agent, "");
		}
		addBooksGui.dispose();
	}

	public void addBookToCatalogue(final String title, final int price) {
		addBehaviour(new OneShotBehaviour() {
                        @Override
			public void action() {
				catalogue.put(title, price);
                                sendLog(LogSeller.Action.addBook, LogSeller.TypeLog.INFO,LogSeller.MethodName.addBookToCatalogue,"109", LogSeller.Resource.book, title+": "+ price);
                                
                                if(new Integer(price) == null){
                                    sendLog(LogSeller.Action.addBook, LogSeller.TypeLog.WARNING,LogSeller.MethodName.addBookToCatalogue,"109", LogSeller.Resource.book, title+": price is null");
                                
                                }
			}
		} );
	}

private class receiveBudgetRequest extends CyclicBehaviour {

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                              
                String title = msg.getContent();
                ACLMessage answer = msg.createReply();
                String clientName = msg.getSender().getLocalName();

                sendLog(LogSeller.Action.receivedBudget, LogSeller.TypeLog.INFO,LogSeller.MethodName.receiveBudgetRequest,"133", LogSeller.Resource.book, clientName);
                Integer price = (Integer) catalogue.get(title);
                if (price != null) {
                    answer.setPerformative(ACLMessage.PROPOSE); 
                    answer.setContent(String.valueOf(price.intValue()));
                  //  sendLog(LogSeller.Action.answeredBudget, LogSeller.TypeLog.INFO,LogSeller.MethodName.receiveBudgetRequest,"133", LogSeller.Resource.book,"client: "+clientName+ " Title: "+title+" Price: "+ price);
                  
                } else {
                    answer.setPerformative(ACLMessage.REFUSE);
                    answer.setContent("Not Available");
                  //  sendLog(LogSeller.Action.refusedBudget, LogSeller.TypeLog.WARNING,LogSeller.MethodName.receiveBudgetRequest,"133", LogSeller.Resource.book,"client: "+clientName+ " Title: "+title+" Price: "+ price);
                  
                }
                myAgent.send(answer);
            } else {
               // sendLog(LogSeller.Action.waitClient, LogSeller.TypeLog.INFO,LogSeller.MethodName.receiveBudgetRequest,"133", LogSeller.Resource.book,"");
                  
                block();
            }
        } //class end
    }  

	private class receiveMsgToBuy extends CyclicBehaviour {
                @Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				String title = msg.getContent();
				ACLMessage answer = msg.createReply();
				Integer price = (Integer) catalogue.remove(title);
                                String clientName = msg.getSender().getLocalName();
                                
                                sendLog(LogSeller.Action.receivedBuy, LogSeller.TypeLog.INFO,LogSeller.MethodName.receiveMsgToBuy,"171", LogSeller.Resource.book,"client: "+clientName+ " Title: "+title+" Price: "+ price);       
				
				if (price != null) {
					answer.setPerformative(ACLMessage.INFORM);
                                        sendLog(LogSeller.Action.soldBook, LogSeller.TypeLog.INFO,LogSeller.MethodName.receiveMsgToBuy,"171", LogSeller.Resource.book,"client: "+clientName+ " Title: "+title+" Price: "+ price);   
                                        booksToPost.put(clientName, title);
                                        
				}
				else {
					answer.setPerformative(ACLMessage.FAILURE);
					answer.setContent("not-available");
                                        sendLog(LogSeller.Action.refuseSell, LogSeller.TypeLog.WARNING,LogSeller.MethodName.receiveMsgToBuy,"171", LogSeller.Resource.book,"client: "+clientName+ " Title: "+title+" Price: "+ price);       
				
				}
                              //  takeDown();
				myAgent.send(answer);
			}
			else {
				block();
			}
		}
                
	}  //class end
        
        public class sendBooksToPostOffice extends TickerBehaviour{

            
            public sendBooksToPostOffice(Agent a, long period) {
                super(a, period);
            }

            @Override
            protected void onTick() {
              Iterator it = booksToPost.entrySet().iterator();
              while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                String clientName = (String) pair.getKey();
                String bookName = (String) pair.getValue();
                postman.addBookToDelivery(clientName, bookName);
                sendLog(LogSeller.Action.postedBook, LogSeller.TypeLog.INFO,LogSeller.MethodName.sendBooksToPostOffice,"220", LogSeller.Resource.book, clientName+": "+ bookName);

                it.remove(); // avoids a ConcurrentModificationException
                booksToPost.remove(clientName);
               }                      
            }   
        }//class end
        
        public class DefectsendBooksToPostOffice extends TickerBehaviour{

            public DefectsendBooksToPostOffice(Agent a, long period) {
                super(a, period);
            }

            @Override
            protected void onTick() {
              Iterator it = booksToPost.entrySet().iterator();
              while (it.hasNext()) {
                  
               /******* MUTATION ____*///   
                  takeDown();
                  
               
              /*  Map.Entry pair = (Map.Entry)it.next();
                String clientName = (String) pair.getKey();
                String bookName = (String) pair.getValue();
                postman.addBookToDelivery(clientName, bookName);
                sendLog(LogSeller.Action.postedBook, LogSeller.TypeLog.INFO,LogSeller.MethodName.sendBooksToPostOffice,"220", LogSeller.Resource.book, clientName+": "+ bookName);

                it.remove(); // avoids a ConcurrentModificationException
                booksToPost.remove(clientName);*/
               }                      
            }    
        }//class end
        
        
    @Override
    public String getNameClass() {
        return this.getClass().getSimpleName();    
    }
}