/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01.agents.seller;

import br.pucrio.les.mas.publisher.framework.LogValues;

/**
 *
 * @author nathalianascimento
 */
public class LogSeller implements LogValues{
    
   public enum Action implements LogValues.Action {
        create,connectToSystem,createCatalogue,registerService,addBook,receivedBudget,
        answeredBudget, refusedBudget, receivedBuy,soldBook,
        removeBook,refuseSell,postedBook,isDestroyed,waitClient;
    }
    public enum TypeLog implements LogValues.TypeLog{
        INFO, WARNING, ERROR;
    }
    public enum Resource implements LogValues.Resource{
         catalogue,book,yellowpage,agent,none;
    }
    public enum MethodName implements LogValues.MethodName{
        initAgent,setup,takeDown,addBookToCatalogue, receiveBudgetRequest, 
        receiveMsgToBuy,registerService,sendBooksToPostOffice;
    }
}
