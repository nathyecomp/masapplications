/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01.agents.client;

import br.pucrio.les.mas.publisher.framework.LogValues;

/**
 *
 * @author nathalianascimento
 */
public class LogClient implements LogValues{
    
   public enum Action implements LogValues.Action {
        create,connectToSystem,searchBook,receiveListFromDF,isDestroyed,askPrice,
        receivedBudget,receivedRefusedBudget,waitSeller,selectBestPrice, askBuy, receivedSaleConfirmation,receivedBook,
        addReceiver;
    }
    public enum TypeLog implements LogValues.TypeLog{
        INFO, WARNING, ERROR;
    }
    public enum Resource implements LogValues.Resource{
         book,yellowpage,agent,none;
    }
    public enum MethodName implements LogValues.MethodName{
        initAgent,setup,takeDown,buyBook,done,getBookInPostOffice;
    }
}
