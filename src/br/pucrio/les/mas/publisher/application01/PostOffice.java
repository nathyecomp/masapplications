/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01;

import br.pucrio.les.mas.publisher.application01.exceptions.BookIsNotInPostOffice;
import br.pucrio.les.mas.publisher.application01.exceptions.NothingForClientInPostOffice;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


/**
 *
 * @author nathalianascimento
 */
public class PostOffice {
    private HashMap booksDelivery;
    
    public static PostOffice instance;
    
  /*   public static PostOffice getIntsance(){
              return instance;
     }*/

    private PostOffice(){
        booksDelivery = new HashMap();
    }
    
    public static synchronized PostOffice getInstance()
     {
         if(instance==null)
         {
             instance=new PostOffice();
         }
         return instance;
     }
    
    
    
    public void addBookToDelivery(String nameClient, String nameBook){
        //a client can request more than one book.
        
        List booksPerClient = new ArrayList();
        if(booksDelivery.containsKey(nameClient)){
            booksPerClient.addAll((Collection) booksDelivery.get(nameClient));
            booksPerClient.add(nameBook);
        }
        else {
            booksPerClient = new ArrayList();
            booksPerClient.add(nameBook);
        }
        booksDelivery.put(nameClient, booksPerClient);
    }
    
    public List removeBookList(String nameClient) throws NothingForClientInPostOffice{
        if (booksDelivery.containsKey(nameClient)){
            return (List) booksDelivery.remove(nameClient);
        }
        
        else{
            throw new NothingForClientInPostOffice(nameClient);
        }
    }
    
    public String removeBook(String nameClient, String nameBook) throws BookIsNotInPostOffice, NothingForClientInPostOffice{
        if (booksDelivery.containsKey(nameClient)){
            List laux = (List) booksDelivery.remove(nameClient);
            if(laux.contains(nameBook)){
                laux.remove(nameBook);
                booksDelivery.put(nameClient, laux);
                return nameBook;
            }
            else throw new BookIsNotInPostOffice(nameClient, nameBook);
        }
         else{
            throw new NothingForClientInPostOffice(nameClient);
        }
    }
}
