/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01.exceptions;

import br.pucrio.les.mas.publisher.framework.TestingException;

/**
 *
 * @author nathalianascimento
 */
public class AddReceiverForACLMsg extends TestingException{
    
    
    public AddReceiverForACLMsg(String nameAgent) {
        super(nameAgent+ " was not added");
    }
   /* public NothingForClientInPostOffice(String msg) {
        super(msg);
    }*/
    
}
