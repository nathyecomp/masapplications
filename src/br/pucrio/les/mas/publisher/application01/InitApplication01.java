/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.application01;

import br.pucrio.les.mas.publisher.application01.agents.client.ClientAgent;
import br.pucrio.les.mas.publisher.application01.agents.client.LogClient;
import br.pucrio.les.mas.publisher.application01.agents.seller.LogSeller;
import br.pucrio.les.mas.publisher.application01.agents.seller.SellerAgent;
import br.pucrio.les.mas.publisher.application01.agents.seller.SellerAgentMutant;
import jade.wrapper.StaleProxyException;
import br.pucrio.les.mas.publisher.framework.InitAgent;

/**
 *
 * @author nathalianascimento
 */
public class InitApplication01 {
 
    /**
    * Start the main agents
    * @param ip
    * @param typeMsgController 
    */
   public static void start01_Integration(){
       
       SellerAgent seller0 = new SellerAgent(false);
       SellerAgent seller1 = new SellerAgent(true);
       //SellerAgent seller2 = new SellerAgent(true); 
       ClientAgent client1 = new ClientAgent("book1");
       
       //initNumSellerAgents(50);
       try {
            InitAgent.init(seller0, "seller0", "SELLER");
            seller0.sendLog(LogSeller.Action.create, LogSeller.TypeLog.INFO,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
            seller0.addBookToCatalogue("book1", 200);                   
        } catch (StaleProxyException ex) {
            seller0.sendLog(LogSeller.Action.create, LogSeller.TypeLog.ERROR,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
        }
        try {
            InitAgent.init(seller1, "seller1", "SELLER");
            seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.INFO,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
              seller1.addBookToCatalogue("book1", 100);
                                
        } catch (StaleProxyException ex) {
            seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.ERROR,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
        }
//        try {
//            InitAgent.init(seller2, "seller2", "SELLER");
//            seller2.sendLog(LogSeller.Action.create, LogSeller.TypeLog.INFO,LogSeller.MethodName.initAgent,"46", LogSeller.Resource.agent, "");
//              seller2.addBookToCatalogue("book1", 10);
//        } catch (StaleProxyException ex) {
//           seller2.sendLog(LogSeller.Action.create, LogSeller.TypeLog.ERROR,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
//        }
        try {
            InitAgent.init(client1, "client1", "CLIENT");
            client1.sendLog(LogClient.Action.create, LogClient.TypeLog.INFO,LogClient.MethodName.initAgent,"54", LogClient.Resource.agent, "");
                                
        } catch (StaleProxyException ex) {
            client1.sendLog(LogClient.Action.create, LogClient.TypeLog.ERROR,LogClient.MethodName.initAgent,"54", LogClient.Resource.agent, "");

        }
       
   }
   
    /**
    * Start the main agents
    * @param ip
    * @param typeMsgController 
    */
   public static void start01_Performance(int numSellers){
       
       SellerAgent seller1 = new SellerAgent(false);
       ClientAgent client1 = new ClientAgent("book1");
       
       initNumSellerAgents(numSellers-1);
        try {
            InitAgent.init(seller1, "seller"+numSellers, "SELLER");
            seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.INFO,LogSeller.MethodName.initAgent,"80", LogSeller.Resource.agent, "");
            seller1.addBookToCatalogue("book1", 10);
        } catch (StaleProxyException ex) {
            seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.ERROR,LogSeller.MethodName.initAgent,"80", LogSeller.Resource.agent, "");
        }
        try {
            InitAgent.init(client1, "client1", "CLIENT");
            client1.sendLog(LogClient.Action.create, LogClient.TypeLog.INFO,LogClient.MethodName.initAgent,"84", LogClient.Resource.agent, "");
                                
        } catch (StaleProxyException ex) {
            client1.sendLog(LogClient.Action.create, LogClient.TypeLog.ERROR,LogClient.MethodName.initAgent,"84", LogClient.Resource.agent, "");

        }
       
   }
   
      public static void initNumSellerAgents(int num){
       for(int i=1; i<=num; i++){
           SellerAgent seller1 = new SellerAgent(false);
           try {
               InitAgent.init(seller1, "seller"+i, "SELLER");
               seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.INFO,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");
             
           } catch (StaleProxyException ex) {
               seller1.sendLog(LogSeller.Action.create, LogSeller.TypeLog.ERROR,LogSeller.MethodName.initAgent,"40", LogSeller.Resource.agent, "");

           }
       }
   }
}
