/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.framework;

/**
 *
 * @author nathalianascimento
 */
public class TestingException extends Exception {

    private String msg;
    LogContext logc = new LogContext();
    
    
    public TestingException(String msg, TestableAgent agent, String action, String resource){
      super(msg);
      this.msg = msg;
    }
    
    public TestingException(String msg){
      super(msg);
      this.msg = msg;
    }
    
    public String getMessage(){
      return msg;
    }
}
