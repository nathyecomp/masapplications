/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrio.les.mas.publisher.framework;

//import agents.SocketProxyAgent.SocketProxyAgent;
import jade.core.Agent;
import jade.wrapper.ControllerException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Nathalia
 */
public abstract class TestableAgent extends Agent {
    
    boolean testMode = true;
    
    String agentType;
    String agentName;
    String year,month,day,hour,minute,second,millisecond;
    
    public abstract String getNameClass();
    
    
    public TestableAgent(){
        
    }
    
    public void sendLog(LogValues.Action action, LogValues.TypeLog typeLog, 
            LogValues.MethodName methodName, 
            String codeLine, LogValues.Resource resource, String message){
        this.sendLog(action.name(), typeLog.name(), methodName.name(), codeLine, resource.name(), message);
    }
    private void sendLog(String action, String typeLog, String methodName, 
            String codeLine, String resource, String message){
   // public void sendLog(Action action, TypeLog typeLog, MethodName methodName, 
     //       String codeLine, Resource resource, String message){
        LogContext newLog = new LogContext();
        refreshTime();
        
        try {
            this.agentName = this.getAID().getLocalName();//agentId;
            this.agentType = this.getContainerController().getContainerName();
        } catch (ControllerException ex) {
            Logger.getLogger(TestableAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        newLog.setClassName(this.getNameClass());
        newLog.setAgentName(agentName);
        newLog.setAgentType(agentType);
        newLog.setYear(year);
        newLog.setMonth(month);
        newLog.setDay(day);
        newLog.setHour(hour);
        newLog.setMinute(minute);
        newLog.setSecond(second);
        newLog.setMillisecond(millisecond);
        
        newLog.setAction(action);
        newLog.setTypeLog(typeLog);
        newLog.setMethodName(methodName);
        newLog.setCodeLine(codeLine);
        newLog.setResource(resource);
        newLog.setMessage(message);
        send(newLog);
    }

    private void send(LogContext newLog) {
        if(testMode ==true){
        
        try {
           String dateInFormat = getDateInFormat(newLog);
           String log[] = {newLog.getAgentType()+ "." + newLog.getAgentName() + "." +
                   newLog.getAction() + "." +
                    newLog.getTypeLog() + "." + newLog.getClassName()+ "." +
                    newLog.getMethodName() + "." + newLog.getCodeLine()+ "." +
                    newLog.getResource() + "." + dateInFormat + "." + newLog.getMessage()};
            RabbitMQ.sendMessage(log);
        } catch (Exception ex) {
            Logger.getLogger(TestableAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        }

    }
    
    private String getDateInFormat(LogContext newLog){
        String dateInFormat = newLog.getYear()+ "." +newLog.getMonth()+ "." +newLog.getDay()+ "." +
                newLog.getHour()+ "." +newLog.getMinute()+ "." +newLog.getSecond()+ "." +newLog.getMillisecond();
        return dateInFormat;
    }
    
    private void refreshTime(){
        Calendar cal = Calendar.getInstance();
        year = String.valueOf(cal.get(Calendar.YEAR));
        month = String.valueOf( cal.get(Calendar.MONTH)+1); //zero-based
        day = String.valueOf( cal.get(Calendar.DAY_OF_MONTH));
        hour = String.valueOf( cal.get(Calendar.HOUR_OF_DAY));
        minute = String.valueOf( cal.get(Calendar.MINUTE));
        second = String.valueOf( cal.get(Calendar.SECOND));
        millisecond = String.valueOf( cal.get(Calendar.MILLISECOND));
    }
    
    public String getYear(){
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }

    public String getHour() {
        return hour;
    }

    public String getMinute() {
        return minute;
    }

    public String getSecond() {
        return second;
    }

    public String getMillisecond() {
        return millisecond;
    }
    
    
    
    
}
