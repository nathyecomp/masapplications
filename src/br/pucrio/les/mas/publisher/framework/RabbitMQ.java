/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.publisher.framework;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
/**
 *
 * @author nathalianascimento
 */
public class RabbitMQ {
    private static final String EXCHANGE_NAME = "topic_logs";
    public static void sendMessage(String msg[])
            throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        String routingKey = getRouting(msg);
        String message = getMessage(msg);

        channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
        //System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
        System.out.println(" [x] Sent '" + message );

        //  channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        // String queueName = channel.queueDeclare().getQueue();
        connection.close();
    }
    
    private static String getRouting(String[] strings) {
        if (strings.length < 1) {
            System.out.println("No severity");
            return "No severity was informed!";
        }
        // System.out.println("Yes Severity");
        String temp = joinStrings(strings, " ");
        String[] tempsplit = temp.split(" ");

        String severity = tempsplit[0]; // error, info or warning
        //  System.out.println(severity);
        return severity;
    }

    private static String getMessage(String[] strings) {
        if (strings.length < 1) {
            return "Hello World!";
        }
        return joinStrings(strings, " ");
    }

    private static String joinStrings(String[] strings, String delimiter) {
        int length = strings.length;
        if (length == 0) {
            return "";
        }
        StringBuilder words = new StringBuilder(strings[0]);
        for (int i = 1; i < length; i++) {
            words.append(delimiter).append(strings[i]);
        }
        return words.toString();
    }
}
